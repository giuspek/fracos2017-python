from dwave.cloud import Client
from dwave.system import DWaveSampler, EmbeddingComposite, AutoEmbeddingComposite, FixedEmbeddingComposite
from copy import deepcopy
import dwave_networkx as dnx
from dwave.embedding import embed_ising
import subprocess
import networkx as nx
import numpy as np
import dimod
from collections import defaultdict
from hybrid.reference.kerberos import KerberosSampler
from dwave.system import LeapHybridSampler
from dwave.embedding import unembed_sampleset
import minorminer
from math import sqrt
import dwave.inspector as inspector
import argparse

parser = argparse.ArgumentParser(description='Execute the fracos2017 examples on a DWave quantum annealer')
parser.add_argument('input', help='Name of the problem you want to test (eg. sgen-twoinfour-s80-g4-0)')
parser.add_argument('--help', help='Show list of arguments accepted by the code')

solver = DWaveSampler(solver={'chip_id':'DW_2000Q_VFYC_6'})


def getMapIndex(benchFile):
    file = open(benchFile, "r")
    lines = file.readlines()
    lines = lines[1:]
    mapping = dict()
    already_checked = set()
    index = 0
    for line in lines:
        temp = line[10:-3]
        var = temp.split(" ")
        var = [int(x.strip(",")) for x in var]
        var = var[1:] + [var[0]]
        for i in var:
            if i not in already_checked:
                mapping[index] = i
                index += 1
                already_checked.add(i)
    return mapping 


def importEmb(embeddingFile):
    file = open(embeddingFile, "r")
    lines = file.readlines()
    counter = 0
    i = 0
    emb = dict()
    chains = dict()
    for line in lines:
        if line[0] != "c":
            data = line.strip().split(" ")
            data = [int(x) for x in data]
            counter += len(data)  
            emb[i] = data
            if len(data) > 1:
                chains[i] = data
            i += 1

    return emb, chains


def readIsing(isingFile):
    file = open(isingFile, "r")
    lines = file.readlines()
    h, J = dict(), dict()
    first = True
    for line in lines:
        if line[0] != "c":
            if first:
                first = False
            else:
                data = line.strip().split(" ")
                data = [int(el) for el in data]
                if data[0] == data[1]:
                    h[data[0]] = data[2]
                else:
                    J[(min(data[0],data[1]), max(data[0], data[1]))] = data[2]

    return h, J


def run2in4BenchmarksDemo(isingFile,embeddingFile, benchFile):   
    h0,J0 = readIsing(isingFile);
    mapping = getMapIndex(benchFile)

    emb, chains = importEmb(embeddingFile);

    h, J = embed_ising(h0, J0, emb, solver.adjacency, chain_strength=1)
    bqm = dimod.BinaryQuadraticModel.from_ising(h0, J0)

    print("Sampling started...")
    resp = solver.sample_ising(h, J, num_reads = 100, num_spin_reversal_transforms=10)
    resp = unembed_sampleset(resp, emb, bqm)

    print("Sampling done! Best result is the following:")
    inspector.show(resp)
    resp = [(resp.first.sample, resp.first.energy)]
    print(resp)
    test_result(benchFile, chains, resp, mapping)
    

def test_result(benchFile, chains, resp, mapping):
    file = open(benchFile, "r")
    rows = list()
    for sample, energy in resp:
        i = 0
        for var in sample:
            if i >= max_i:
                break
            if sample[var] == 1:
                row = "(define-fun v{} () Bool true)".format(mapping[var])
            else:
                row = "(define-fun v{} () Bool false)".format(mapping[var])
            rows.append(row)
            i += 1
    lines = file.readlines()
    lines = lines[1:]
    for line in lines:
        temp = line[10:-3]
        var = temp.split(" ")
        var = [int(x.strip(",")) for x in var]
        term = "(assert (or "
        sets = [(0,1,2,3),(0,2,1,3),(0,3,1,2),(1,2,0,3),(1,3,0,2),(2,3,0,1)]
        for index in sets:
            term = term + "(and v{} v{} (not v{}) (not v{}))".format(var[index[0]],var[index[1]],var[index[2]],var[index[3]])
        term = term + "))"
        rows.append(term)
    rows.append("(check-sat)")
    inputfile = open("input.smt", "w")
    inputfile.write("\n".join(rows))
    inputfile.close()
    inputfile = open("input.smt", "r")
    # TODO: update the path so that is refers to a local and updated version of OptiMathSAT
    res = subprocess.call("INSERT-PATH-HERE", stdin=inputfile)
    print("***************************************************")
    print("Output: {}".format(res))   
    print("***************************************************")


args = parser.parse_args()
problem = args.input
max_i = 80
run2in4BenchmarksDemo('allbenchmarks_ising/{}.ising'.format(problem), 'allbenchmarks_ising/{}.embedding'.format(problem), 'allbenchmarks/{}.bench'.format(problem))
