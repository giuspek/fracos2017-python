import argparse
from collections import defaultdict

parser = argparse.ArgumentParser(description='Generate the Ising model from a TwoInFour problem')
parser.add_argument('input', help='Name of the problem you want to test (eg. sgen-twoinfour-s80-g4-0.bench)')
parser.add_argument('output', help='Name of the output file that will store the Ising model (by default printed on terminal)')

args = parser.parse_args()
problem = args.input


def generateIsing(input_file_name, output_file_name):
    input_file = open(input_file_name, "r")
    output_file = open(output_file_name, "w")

    lines = input_file.readlines()
    solution_line = lines[0].strip()
    solution_line_split = solution_line.strip().split(" ")

    max_index = abs(int(solution_line_split[-1]))
    ancilla_index = max_index + 1
    var_index = 0

    var_mapping = dict()
    ising_model = defaultdict(int)
    n_constraints = 0
    for line in lines[1:]:
        assert line[:9] == "TWOINFOUR", "The file is not a TWOINFOUR problem"
        n_constraints += 1
        temp = line[10:-3]
        variables = temp.split(" ")
        variables = [int(x.strip(",")) for x in variables]
        variables = variables[1:] + [variables[0]]
        ancillas = [ancilla_index, ancilla_index+1]

        for var in variables:
            if var not in var_mapping:
                var_mapping[var] = var_index
                var_index = var_index + 1

        ising_model[(min(var_mapping[variables[0]], var_mapping[variables[1]]), max(var_mapping[variables[0]], var_mapping[variables[1]]))] += 1
        ising_model[(min(var_mapping[variables[0]], var_mapping[variables[2]]), max(var_mapping[variables[0]], var_mapping[variables[2]]))] += 1
        ising_model[(min(var_mapping[variables[3]], var_mapping[variables[1]]), max(var_mapping[variables[3]], var_mapping[variables[1]]))] += 1
        ising_model[(min(var_mapping[variables[3]], var_mapping[variables[2]]), max(var_mapping[variables[3]], var_mapping[variables[2]]))] += 1
        ising_model[(min(var_mapping[variables[1]], ancillas[0]), max(var_mapping[variables[1]], ancillas[0]))] += -1
        ising_model[(min(var_mapping[variables[2]], ancillas[0]), max(var_mapping[variables[2]], ancillas[0]))] += 1
        ising_model[(min(var_mapping[variables[0]], ancillas[1]), max(var_mapping[variables[0]], ancillas[1]))] += 1
        ising_model[(min(var_mapping[variables[3]], ancillas[1]), max(var_mapping[variables[3]], ancillas[1]))] += -1
        ancilla_index = ancilla_index + 2

    output_file.write("c FILE: {}\n".format(args.input))
    output_file.write("c  DESCRIPTION: Ising model representation of a 2-in-4-SAT problem\n")
    output_file.write('c  NOTE: Minimum energy = {}\n'.format(-n_constraints*4))
    output_file.write('c\n')
    output_file.write('c  This ising file has the following format.\n')
    output_file.write('c  Lines starting with c are comments to be ignored\n')
    output_file.write('c  First non-comment line: (# of qubits) (# of h and J terms)\n')
    output_file.write('c  h lines have the form: i i h_i\n')
    output_file.write('c  J lines have the form: i j J_{ij}\n')
    output_file.write('c\n')
    output_file.write('{} {}\n'.format(ancilla_index, len(ising_model)))
    for pair in ising_model:
        output_file.write("{} {} {}\n".format(pair[0], pair[1], ising_model[pair]))

generateIsing(problem, args.output)