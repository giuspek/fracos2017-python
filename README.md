# Fracos2017-Python

Repository containing experimental data and scripts to perform testing on te paper **Solving SAT and MaxSAT with a Quantum Annealer: Foundations and a Preliminary Report**.

## Structure of the repository

- **allbenchmarks/**: directory containing SAT problems, both in .bench and .cnf format.
- **allbenchmarks_ising**: directory containing a pre-computed Ising model and quantum placements for each TWO-IN-FOUR problem contained in allbenchmarks.
- **output2in4Ising**: given an input file (.bench), it generates an output file containing the Ising model describing the problem.
- **run2in4benchmark**: given a TWO-IN-FOUR input file (.bench), it uses the pre-computed Ising and embedding data to place it into a Chimera architecture, enabling the user to sample some solutions and check their correctness.
